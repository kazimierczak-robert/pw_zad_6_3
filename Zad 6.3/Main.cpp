#include <iostream>
#include <fstream>
#include <string>
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#include <CL/cl.h>
using namespace std;

void randomizeVector(cl_int* tab, size_t vectorSize)
{
	for (size_t i = 0; i < vectorSize; i++)
	{
		tab[i] = rand() % 10;
	}
}

int main()
{
	cl_int error = CL_SUCCESS;

	// Get platform number.
	cl_uint platformNumber = 0;
	error = clGetPlatformIDs(0, NULL, &platformNumber);
	if (0 == platformNumber)
	{
		cout << "No OpenCL platforms found." << endl;
		return 0;
	}

	// Get platform identifiers.
	cl_platform_id* platformIds = new cl_platform_id[platformNumber];
	error = clGetPlatformIDs(platformNumber, platformIds, NULL);

	// Get device count.
	cl_uint deviceNumber;
	error = clGetDeviceIDs(platformIds[0], CL_DEVICE_TYPE_ALL, 0, NULL, &deviceNumber);
	if (0 == deviceNumber)
	{
		cout << "No OpenCL devices found on platform " << 1 << "." << endl;
	}

	// Get device identifiers.
	cl_device_id* deviceIds = new cl_device_id[deviceNumber];
	error = clGetDeviceIDs(platformIds[0], CL_DEVICE_TYPE_GPU, deviceNumber, deviceIds, &deviceNumber);

	// Allocate and initialize host arrays	
	size_t localWorkSize = 4;
	size_t vectorSize = localWorkSize*localWorkSize;

	cl_int* a = new cl_int[vectorSize];
	cl_int* b = new cl_int[vectorSize];
	cl_int* c = new cl_int[vectorSize];
	randomizeVector(a, vectorSize);
	randomizeVector(b, vectorSize);
	for (size_t i = 0; i < vectorSize; i++)
	{
		c[i]=0;
	}

	// Create the OpenCL context.
	cl_context context = clCreateContext(0, deviceNumber, deviceIds, NULL, NULL, NULL);

	if (NULL == context)
	{
		cout << "Failed to create OpenCL context." << endl;
	}
	// Create a command-queue
	cl_command_queue commandQueue = clCreateCommandQueue(context, deviceIds[0], 0, &error);
	// Allocate the OpenCL buffer memory objects for source and result on the device.
	cl_mem bufferA = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_int) * vectorSize, NULL, &error);
	cl_mem bufferB = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_int) * vectorSize, NULL, &error);
	cl_mem bufferC = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_int) * vectorSize, NULL, &error);

	// Read the OpenCL kernel in from source file.
	ifstream file(".\\Multiply.cl", ifstream::in);
	string str;

	str.assign(istreambuf_iterator<char>(file), istreambuf_iterator<char>());
	const char* source = str.c_str();

	// Create the program.
	cl_program program = clCreateProgramWithSource(context, 1, &source, NULL, &error);
	error = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);

	// Create the kernel.
	cl_kernel kernel = clCreateKernel(program, "Multiply", &error);

	// Set the Argument values.
	error = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&bufferA);
	error = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)&bufferB);
	error = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void*)&bufferC);
	error = clSetKernelArg(kernel, 3, sizeof(cl_int), (void*)&localWorkSize);

	// Asynchronous write of data to GPU device.
	error = clEnqueueWriteBuffer(commandQueue, bufferA, CL_FALSE, 0, sizeof(cl_int) * vectorSize, a, 0, NULL, NULL);
	error = clEnqueueWriteBuffer(commandQueue, bufferB, CL_FALSE, 0, sizeof(cl_int) * vectorSize, b, 0, NULL, NULL);

	// Launch kernel.
	error = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, &vectorSize, &localWorkSize, 0, NULL, NULL);

	// Read back results and check accumulated errors.
	error = clEnqueueReadBuffer(commandQueue, bufferC, CL_TRUE, 0, sizeof(cl_int) * vectorSize, c, 0, NULL, NULL);

	// Print results.
	cout << "Macierz A\n";
	for (size_t i = 0; i < vectorSize; i++)
	{
		cout << a[i];
		if (i % localWorkSize == localWorkSize - 1) cout << "\n";
		else cout << "\t";
	}
	cout << "\n";
	cout << "Macierz B\n";
	for (size_t i = 0; i < vectorSize; i++)
	{
		cout << b[i];
		if (i % localWorkSize == localWorkSize - 1) cout << "\n";
		else cout << "\t";
	}
	cout << "\n";
	cout << "Macierz C\n";
	for (size_t i = 0; i < vectorSize; i++)
	{
		cout << c[i];
		if (i % localWorkSize == localWorkSize - 1) cout << "\n";
		else cout << "\t";
	}
	cout << "\n";

	// Cleanup and free memory.
	clFlush(commandQueue);
	clFinish(commandQueue);
	clReleaseKernel(kernel);
	clReleaseProgram(program);
	clReleaseMemObject(bufferA);
	clReleaseMemObject(bufferB);
	clReleaseMemObject(bufferC);
	clReleaseCommandQueue(commandQueue);
	clReleaseContext(context);

	delete[] a;
	delete[] b;
	delete[] c;

	delete[] platformIds;
	delete[] deviceIds;

	system("pause");
	return 0;
}